function uploadData() {


    // TODO: Replace the following with your app's Firebase project configuration
    const firebaseConfig = {
        apiKey: "AIzaSyArHPMihVQld7zQpUOk3XUGNYTW-0Otu3Y",
        authDomain: "nebus-c12ec.firebaseapp.com",
        databaseURL: "https://nebus-c12ec.firebaseio.com",
        projectId: "nebus-c12ec",
        storageBucket: "nebus-c12ec.appspot.com",
        messagingSenderId: "236610051961",
        appId: "1:236610051961:web:07d436fb5162e5b478d9a8"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    var database = firebase.firestore()

    // let dataFromDb = database.collection('bus_stops').get().then( busStopsInfo => {
    //     busStopsInfo.docs.map(busStop => {
    //         console.log("id")
    //         console.log(busStop.id)
    //         console.log("data")
    //         console.log(busStop.data())
    //     })
    // })

    // database.collection('bus_stops').doc("123").set({
    //     location: new firebase.firestore.GeoPoint(-74.0447001153344, 4.764414285928249)
    // })

    // var dataToExport = []
    // //Read 
    // d3.csv("paraderos_18-3.csv").then(
    //     (dataReaded) => {
    //         dataReaded.map(busStop => {
    //             let longitude = parseFloat(busStop.longitude);
    //             let latitude = parseFloat(busStop.latitude);
    //             let id = busStop.id;
    //             database.collection('bus_stops').doc(id).set({
    //                 location: new firebase.firestore.GeoPoint(latitude, longitude)
    //             })
    //         });
    //     });


    //database.collection('bus_stops').doc('PIR_CL191').set('18-3 Universidades')
    // database.collection('bus_stops').doc('344A00').collection('18-3 Universidades').get().then( busStopInfo => {
    //     console.log(busStopInfo.docs.length)
    //     console.log(busStopInfo.docs)
    //     busStopInfo.docs.map(busStop => {
    //         console.log(busStop.data())
    //     })
    // })

    // database.collection('bus_stops').doc('343A00').collection('18-3 Universidades').doc("1").set({
    //     arrivesIn : 7,
    //     course : 202,
    //     location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
    //     speed : 12
    // })

    //Read the csv and create the subcollections
    // d3.csv("paraderos_18-3.csv").then(
    //     (dataReaded) => {
    //         dataReaded.map((busStop, iterator) => {
    //             let id = busStop.id
    //             if (iterator<71) {
    //                 database.collection('bus_stops').doc(id).collection('18-3 Universidades').doc("1").set({
    //                     arrivesIn : 7,
    //                     course : 202,
    //                     location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
    //                     speed : 12
    //                 })
    //             } else {
    //                 database.collection('bus_stops').doc(id).collection('18-3 Auto Norte').doc("1").set({
    //                     arrivesIn : 7,
    //                     course : 202,
    //                     location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
    //                     speed : 12
    //                 })
    //             }
    //         });
    //     });

    //Add the bus routes to the bus stops
    d3.csv("paraderos_18-3.csv").then(
        (dataReaded) => {
            dataReaded.map((busStop, iterator) => {
                let id = busStop.id;
                let longitude = parseFloat(busStop.longitude);
                let latitude = parseFloat(busStop.latitude);
                if (iterator < 69) {
                    database.collection('bus_stops').doc(id).set({
                        route: "18-3 Universidades",
                        location: new firebase.firestore.GeoPoint(latitude, longitude)
                    })
                    database.collection('bus_stops').doc(id).collection('18-3 Universidades').doc("1").set({
                        arrivesIn : 7,
                        course : 202,
                        location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
                        speed : 12
                    })
                    database.collection('bus_stops').doc(id).collection('18-3 Universidades').doc("2").set({
                        arrivesIn : 14,
                        course : 202,
                        location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
                        speed : 12
                    })
                    database.collection('bus_stops').doc(id).collection('18-3 Universidades').doc("3").set({
                        arrivesIn : 24,
                        course : 202,
                        location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
                        speed : 12
                    })
                    database.collection('bus_stops').doc(id).collection('18-3 Universidades').doc("4").set({
                        arrivesIn : 36,
                        course : 202,
                        location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
                        speed : 12
                    })
                } else {
                    database.collection('bus_stops').doc(id).set({
                        route: "18-3 Auto Norte",
                        location: new firebase.firestore.GeoPoint(latitude, longitude)
                    })
                    database.collection('bus_stops').doc(id).collection('18-3 Auto Norte').doc("1").set({
                        arrivesIn : 8,
                        course : 202,
                        location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
                        speed : 12
                    })
                    database.collection('bus_stops').doc(id).collection('18-3 Auto Norte').doc("2").set({
                        arrivesIn : 16,
                        course : 202,
                        location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
                        speed : 12
                    })
                    database.collection('bus_stops').doc(id).collection('18-3 Auto Norte').doc("3").set({
                        arrivesIn : 28,
                        course : 202,
                        location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
                        speed : 12
                    })
                    database.collection('bus_stops').doc(id).collection('18-3 Auto Norte').doc("4").set({
                        arrivesIn : 35,
                        course : 202,
                        location: new firebase.firestore.GeoPoint(4.67932, -74.0388),
                        speed : 12
                    })
                }
            });
        });

    // database.collection('bus_stops').get().then( snap => {
    //     console.log(snap.size)
    // });
}
