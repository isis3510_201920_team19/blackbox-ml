function uploadUserLocations() {

    var r = []
    //First from home to anywhere 
    d3.csv("FromAndesToHome.csv").then(
        (dataReaded) => {
            dataReaded.map(locations => {
                var re = JSON.stringify({
                    "timestamp": locations.Timestamp,
                    "latitude": locations.Latitude,
                    "longitude": locations.Longitude,
                    "horizontalAccuracy": locations.HorizontalAccuracy,
                    "course": locations.Course,
                    "speed": locations.Speed
                })
                r.push(re)
            }).then(
                document.getElementById("textico").innerHTML = r
            );
        }
    );
}
